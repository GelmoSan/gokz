"Phrases"
{
	"Block"
	{
		"en"	"Block"
	}
	"Jump Record"
	{
		"#format"	"{1:N},{2:s},{3:.4f},{4:s}"
		"en"		"{lime}{1} {grey}set a new {default}{2} {yellow}PB {grey}of {default}{3} {grey}[{purple}{4}{grey}]"
	}
	"Block Jump Record"
	{
		"#format"	"{1:N},{2:d},{3:s},{4:.4f},{5:s}"
		"en"		"{lime}{1} {grey}set a new {lightgreen}{2} Block {default}{3} {yellow}PB {grey}of {default}{4} {grey}[{purple}{5}{grey}]"
	}
	"Jump Deleted"
	{
		"#format"	"{1:s},{2:s},{3:s},{4:d},{5:d}"
		"en"		"{grey}Deleted the {purple}{1} {lightgreen}{2}{default}{3} record of '{default}STEAM_1:{4}:{5}{grey}'"
	}
	"No SteamID specified"
	{
		"en"		"{darkred}No SteamID specified."
	}
	"Invalid SteamID"
	{
		"en"		"{darkred}Invalid SteamID (Format: STEAM_1:X:X)."
	}
	"Invalid Mode"
	{
		"en"		"{darkred}Invalid mode (Options: SKZ, KZT, VNL)"
	}
	"Invalid Jumptype"
	{
		"en"		"{darkred}Invalid jumptype (Options: LJ, BH, MBH, WJ, LAJ, LAH, JB, LBH, LWJ)"
	}
	"Delete Jump Usage"
	{
		"en"		"Usage: !deletejump <STEAM_1:X:X> <mode> <jump type> <block?>"
	}
	"Set Cheater"
	{
		"#format"	"{1:d},{2:d}"
		"en"		"{grey}SteamID32 '{default}STEAM_1:{1}:{2}{grey}' was set as a cheater."
	}
	"Set Not Cheater"
	{
		"#format"	"{1:d},{2:d}"
		"en"		"{grey}SteamID32 '{default}STEAM_1:{1}:{2}{grey}' was set as not a cheater."
	}
}
